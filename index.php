<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity 1</title>
</head>
<body>
    <h3>Full Address</h3>
    <p><?php echo getFullAddress("Philippines", "Quezon City", "Metro Manila", "3F Caswyn Bldg., Timog Avenue"); ?></p>
    <p><?php echo getFullAddress("Philippines", "Makati City", "Metro Manila", "3F Enzo Bldg., Buendia Avenue"); ?></p>
    <h3>Letter Grade</h3>
    <p><?php echo getLetterGrade(100); ?></p>
    <p><?php echo getLetterGrade(96); ?></p>
    <p><?php echo getLetterGrade(94); ?></p>
    <p><?php echo getLetterGrade(91); ?></p>
    <p><?php echo getLetterGrade(74); ?></p>
    <p><?php echo getLetterGrade(75); ?></p>
    <p><?php echo getLetterGrade(78); ?></p>
    <p><?php echo getLetterGrade(81); ?></p>
    <p><?php echo getLetterGrade(83); ?></p>
    <p><?php echo getLetterGrade(87); ?></p>
</body>
</html>