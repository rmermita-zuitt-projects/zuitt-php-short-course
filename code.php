<?php
function getFullAddress($country, $city, $province, $specificAddress) {
    return "$specificAddress, $city, $province, $country";
}

function getLetterGrade($grade) {
    if($grade >= 98 && $grade <= 100) {
        return "You got an A+ grade";
    }else if($grade >= 95 && $grade <= 97) {
        return "You got an A grade";
    }else if($grade >= 92 && $grade <= 94) {
        return "You got an A- grade";
    }else if($grade >= 89 && $grade <= 91) {
        return "You got a B+ grade";
    }else if($grade >= 86 && $grade <= 88) {
        return "You got a B grade";
    }else if($grade >= 83 && $grade <= 85) {
        return "You got a B- grade";
    }else if($grade >= 80 && $grade <= 82) {
        return "You got a C+ grade";
    }else if($grade >= 77 && $grade <= 79) {
        return "You got a C grade";
    }else if($grade >= 75 && $grade <= 76) {
        return "You got a C- grade";
    }else {
        return "You got a D grade";
    }
}